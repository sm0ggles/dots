```
▓█████▄  ▒█████  ▄▄▄█████▓  █████▒██▓ ██▓    ▓█████   ██████ 
▒██▀ ██▌▒██▒  ██▒▓  ██▒ ▓▒▓██   ▒▓██▒▓██▒    ▓█   ▀ ▒██    ▒ 
░██   █▌▒██░  ██▒▒ ▓██░ ▒░▒████ ░▒██▒▒██░    ▒███   ░ ▓██▄   
░▓█▄   ▌▒██   ██░░ ▓██▓ ░ ░▓█▒  ░░██░▒██░    ▒▓█  ▄   ▒   ██▒
░▒████▓ ░ ████▓▒░  ▒██▒ ░ ░▒█░   ░██░░██████▒░▒████▒▒██████▒▒
 ▒▒▓  ▒ ░ ▒░▒░▒░   ▒ ░░    ▒ ░   ░▓  ░ ▒░▓  ░░░ ▒░ ░▒ ▒▓▒ ▒ ░
 ░ ▒  ▒   ░ ▒ ▒░     ░     ░      ▒ ░░ ░ ▒  ░ ░ ░  ░░ ░▒  ░ ░
 ░ ░  ░ ░ ░ ░ ▒    ░       ░ ░    ▒ ░  ░ ░      ░   ░  ░  ░  
   ░        ░ ░                   ░      ░  ░   ░  ░      ░  
 ░                                                           

 i3             > fast tiling wm
 polybar        > the best status bar
 scripts        > term color, sys info, and other misc scripts
 weechat        > the theme i use
 ncmpcpp        > ncurses mpc++ ui/color settings
 xrdb           > terminal colors, urvxt, and rofi settings
 vim            > plugins and colors and things
 zsh            > zshell settings, aliases, and custom prompts

```
---
# Introduction  
Here is a collection of my dotfiles. I made this to help make it easier for others to get into desktop ricing, just as others have done before me. I always found it nice to have other's setups to look at and steal from so I thought I'd do the same. Enjoy.

# General Screenshot  
![screenshot](screenshot.png)
Weechat, ncmpcpp + Mopidy, and cmatrix.

# Vim and sysinfo  
![vim and sysinfo](vim.png)

# Rofi  
![rofi](rofi.png)

# Todo:
- elaborate in the README how things work
- add more files
